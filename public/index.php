<?php
// constante para definir el path
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../app'));

// separador de directorio
const DS = DIRECTORY_SEPARATOR;

require_once APPLICATION_PATH . DS . 'config' . DS . 'config.php';

$page = get('page', 'home');
$model = $config['MODEL_PATH'] . $page . '.php';
$view =  $config['VIEW_PATH'] . $page . '.php';
$_404 =  "{$config['VIEW_PATH']}404.php";
$_layout = $config['VIEW_PATH'] . DS . 'includes' . DS . 'layout.php';

$main_content = $_404;

if (file_exists($model)) {
    require $model;
}

if (file_exists($view)) {
    $main_content =  $view;
}

include $_layout;
