<h1><?= $title ?></h1>
<?php if(!empty($movies->results)): ?>
<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-5 g-4">
    <?php foreach ($movies->results as $pel): ?>
    <div class="col">
        <div class="card">
            <img class="card-img-top img-thumbnail" src="<?= $url_img . $pel->poster_path ?>" alt="<?= $pel->title ?>">
            <div class="card-body">
                <h5 class="card-title"><?= $pel->title; ?></h5>
                <p class="card-text"><?php echo substr($pel->overview, 0, 200);
                                        echo (strlen($pel->overview) > 200) ? '...' : ''; ?></p>
            </div>
            <div class="card-footer">
                <a href="<?= $config['URL_PATH'] . DS . 'index.php?page=detalle&movie_id=' . $pel->id  ?>" class="btn btn-outline-primary btn-block">Detalle</a>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
<?php else: ?>
    <div class="row">
        <div class="col-12">
            <div class="alert alert-warning" role="alert"><?= $mensaje ?></div>
        </div>
    </div>
<?php endif; ?>
