<?php
$title = "Detalle de la Pelicula";

$url_base = $config['URL_DETALLE'];
$url_img = $config['URL_IMG'];
$api = $config['API_KEY'];
$language = 'es-Mx';
$movie = null;
$movie_id = get('movie_id', '0');
try {
    $mensaje = null;
    $ch = curl_init();
    $url = "{$url_base}/{$movie_id}?api_key={$api}&language={$language}";
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $movie = curl_exec($ch);
    $movie = json_decode($movie);
    // var_dump($movie);
    if (isset($movie->success) && $movie->success == false) {
        $mensaje = $movie->status_message;
        $movie = null;
    }
} catch (\Exception $e) {
    var_dump($e);
} finally {
    curl_close($ch);
}
