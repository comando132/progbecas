<?php
$title = "Peliculas";

$url_base = $config['URL_BASE'];
$url_img = $config['URL_IMG'];
$api = $config['API_KEY'];
$language = 'es-Mx';
$pag = get('pag', 1);
$movies = null;
$mensaje = null;
try {
    $ch = curl_init();
    $url = "{$url_base}/movie/upcoming?api_key={$api}&language={$language}&page={$pag}";
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // texto
    $movies = curl_exec($ch);
    // array
    $movies = json_decode($movies);
    if (empty($movies->results) || (isset($movies->success) && !$movies->success)) {
        $mensaje = ' No se encontraron peliculas';
    }
} catch (\Exception $e) {
    var_dump($e);
} finally {
    curl_close($ch);
}
