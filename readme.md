# MVC con PHP
Ejemplo de código para acomodar en un estilo mvc el código


- Todo pasa por el index.php
- Para agregar una nueva accion se debe crear un archivo en la carpeta model y en la carpeta view.
- En el archivo que se encuentra en la carpeta model se coloca el codigo con toda la lógica. 
- En el archivo que se encuentra en la carpeta view se coloca todo el código relacionado con la vista HTML.
- por url se debe pasar el nombre del archivo creado en el punto anterior  ej: http://127.0.0.1/mvc/public/?page=prueba.
- Se usa una conexion a una api https://api.themoviedb.org/3 con curl.
