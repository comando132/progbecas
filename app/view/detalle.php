<?php if (!empty($movie)) : ?>
    <h1><?= $movie->title ?></h1>
    <div class="row">
        <div class="col-md-4">
            <img src="<?= "$url_img/{$movie->poster_path}"; ?>" class="img-fluid" alt="...">
        </div>
        <div class="col-md-8">
            <p>Titulo Original: <strong><?= $movie->original_title ?></strong></p>
            <p><?= $movie->tagline; ?></p>
            <p> <strong>Géneros:</strong> <br />
            <ul>
                <?php foreach ($movie->genres as $genre) {
                    echo "<li>{$genre->name}</li>";
                } ?>
            </ul>
            </p>
            <p><?= $movie->overview; ?></p>
            <p><strong>Compañias:</strong> <br />
                <?php foreach ($movie->production_companies as $company) {
                    if (!empty($company->logo_path)) {
                        echo "<img src='{$url_img}{$company->logo_path}' width='10%' class='img-fluid img-thumbnail' alt='{$company->name}' title='{$company->name}'>";
                    }
                } ?>
            </p>
            <p><a href="<?= $movie->homepage ?>" class="btn btn-info">Página web</a></p>
        </div>
    </div>
<?php else : ?>
    <div class="row">
        <div class="col-12">
            <div class="alert alert-warning" role="alert"><?= $mensaje ?></div>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-12">
        <a href="index.php" class="btn btn-primary">Regresar</a>
    </div>
</div>
