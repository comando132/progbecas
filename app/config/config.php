<?php
$config = [
    'MODEL_PATH' => APPLICATION_PATH . DS . 'model' . DS,
    'LIB_PATH' => APPLICATION_PATH . DS . 'lib' . DS,
    'VIEW_PATH' => APPLICATION_PATH . DS . 'view' . DS,
];

$config += parse_ini_file(APPLICATION_PATH . '/../.env');

require $config['LIB_PATH'] . 'functions.php';

